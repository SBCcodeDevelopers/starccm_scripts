/* STAR-CCM+ macro: XC_macro.java
 Written by STAR-CCM+ 15.02.007

 ******************************
*        Mio Suzuki 
*        February 16, 2021, v4
*        
*        Procedure:
*        Geometry/Mesh operation
*                  
*                   * frame surface wrapper, cockpit_wrap
*                   * subtract in region 1 (inner region)
*                   * mesh region 1 (surface + volume)
*
*        Run (0 deg)
*        Reporting
*                   * consolidate all values in a single table. extract values. 
*                   * export this table to report.csv
*                   * extract and export local and acc force table to .csv
*                   * print the streamline + vorticity image 
*
*        Rotate the outer mesh by 10 deg + LowRe formulation
*                  * change the iteration stopping criteria
*                  * then execute the same run + reporting 
*                  * print the streamline + vorticity image 
*
* ******************************/

package macro;

import java.util.*;

import java.io.*;
import star.common.*;
import star.base.neo.*;
import star.surfacewrapper.*;
import star.meshing.*;
import star.flow.*;
import star.vis.*;
import star.kwturb.*;

import javax.swing.text.Document;
import org.netbeans.core.output2.ui.AbstractOutputPane;
import org.netbeans.core.output2.ui.AbstractOutputTab;
import org.openide.windows.TopComponent;
import org.netbeans.core.io.ui.IOWindow;

public class XC_macro4 extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    String dir = simulation_0.getSessionDir(); 

    String sep = System.getProperty("file.separator");
    
    String filename = 
      simulation_0.getPresentationName(); 
    
    StepStoppingCriterion stepStoppingCriterion_0 = 
       ((StepStoppingCriterion) simulation_0.getSolverStoppingCriterionManager().getSolverStoppingCriterion("Maximum Steps"));
     
    int itr_num = 300; // max iteration number . For now, 250 for 0 and then 300 for 10
    
    stepStoppingCriterion_0.setMaximumNumberSteps(itr_num);
    
    Region region_0 = 
      simulation_0.getRegionManager().getRegion("Region_2_outer");
    
    Region region_1 = 
      simulation_0.getRegionManager().getRegion("Region_1_inner");

    
//* --------------------------------------------------------------------------------------------------------------------
//0 deg:
     
//Replace the CAD
    
// -------------------------------- new replace (2/16/2021) -----------------    
    
     CompositePart compositePart_1 = 
      ((CompositePart) simulation_0.get(SimulationPartManager.class).getPart("XC"));

    CadPart cadPart_0 = 
      ((CadPart) compositePart_1.getChildParts().getPart("flat_plate_tilted_up"));

    cadPart_0.reimportPart(resolvePath("curved_plate_4_vertical.stp"), 0.0, false);
    
    

     /* Subtract: */
    
                          
     //1.----Subtract, without STwaterbottle-------//
     
     
    
       SubtractPartsOperation subtractPartsOperation_0 = 
      ((SubtractPartsOperation) simulation_0.get(MeshOperationManager.class).getObject("Subtract_region1"));

    subtractPartsOperation_0.getInputGeometryObjects().setQuery(null);

    SimpleCylinderPart simpleCylinderPart_0 = 
      ((SimpleCylinderPart) simulation_0.get(SimulationPartManager.class).getPart("Interface_cylinder"));

    MeshPart meshPart_0 = 
      ((MeshPart) simulation_0.get(SimulationPartManager.class).getPart("wrapped_bottle_DT"));

    CompositePart compositePart_0 = 
      ((CompositePart) simulation_0.get(SimulationPartManager.class).getPart("wrapped_models"));

    MeshOperationPart meshOperationPart_0 = 
      ((MeshOperationPart) compositePart_0.getChildParts().getPart("bar_stem_wrap"));

    MeshOperationPart meshOperationPart_1 = 
      ((MeshOperationPart) compositePart_0.getChildParts().getPart("frame_wrap"));

    MeshOperationPart meshOperationPart_2 = 
      ((MeshOperationPart) compositePart_0.getChildParts().getPart("front_wheel_wrap"));
    

  MeshOperationPart meshOperationPart_3 = 
      ((MeshOperationPart) compositePart_0.getChildParts().getPart("rear_wheel_wrap"));

     
    
    //CompositePart compositePart_1 = 
     // ((CompositePart) simulation_0.get(SimulationPartManager.class).getPart("XC"));

    CadPart cadPart_5 = 
      ((CadPart) compositePart_1.getChildParts().getPart("flat_plate_tilted_up"));
      
    CompositePart compositePart_30 = 
      ((CompositePart) simulation_0.get(SimulationPartManager.class).getPart("CAM_ASM"));

    CadPart cadPart_31 = 
      ((CadPart) compositePart_30.getChildParts().getPart("CAM_SCAN"));

    CadPart cadPart_32 = 
      ((CadPart) compositePart_30.getChildParts().getPart("CAMERHEAD"));


    subtractPartsOperation_0.getInputGeometryObjects().setObjects(compositePart_30, cadPart_31, cadPart_32, simpleCylinderPart_0, meshPart_0, meshOperationPart_0, meshOperationPart_1, meshOperationPart_2, meshOperationPart_3, cadPart_5);
    
    subtractPartsOperation_0.execute();

    

    MeshOperationPart meshOperationPart_4 = 
      ((MeshOperationPart) simulation_0.get(SimulationPartManager.class).getPart("Subtract"));

    PartSurface partSurface_0 = 
      ((PartSurface) meshOperationPart_4.getPartSurfaceManager().getPartSurface("XC.flat_plate_tilted_up.Faces"));

    
    Boundary boundary_0 = 
      region_1.getBoundaryManager().getBoundary("XC.number_plate");

    partSurface_0.setBoundary(boundary_0); 
    
    GeometryObjectProxy geometryObjectProxy_0 = 
      meshOperationPart_4.getOrCreateProxyForObject(cadPart_5);

     
    AutoMeshOperation autoMeshOperation_0 = 
      ((AutoMeshOperation) simulation_0.get(MeshOperationManager.class).getObject("mesh_region1"));

    SurfaceCustomMeshControl surfaceCustomMeshControl_0 = 
      ((SurfaceCustomMeshControl) autoMeshOperation_0.getCustomMeshControls().getObject("plate"));

    surfaceCustomMeshControl_0.getGeometryObjects().setQuery(null);
    surfaceCustomMeshControl_0.getGeometryObjects().setObjects(geometryObjectProxy_0, partSurface_0);


//Mesh

  
   autoMeshOperation_0.execute();
   
    
  // Remove bad cells
      Units units_10 = 
      simulation_0.getUnitsManager().getPreferredUnits(new IntVector(new int[] {0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));

    Units units_11 = 
      simulation_0.getUnitsManager().getPreferredUnits(new IntVector(new int[] {0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));

    Units units_12 = 
      simulation_0.getUnitsManager().hasPreferredUnits(new IntVector(new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));

    MeshManager meshManager_0 = 
      simulation_0.getMeshManager();
    
      meshManager_0.removeInvalidCells(new NeoObjectVector(new Object[] {region_0, region_1}), NeoProperty.fromString("{\'minimumContiguousFaceArea\': 0.0, \'minimumCellVolumeEnabled\': true, \'minimumVolumeChangeEnabled\': true, \'functionOperator\': 0, \'minimumContiguousFaceAreaEnabled\': true, \'minimumFaceValidityEnabled\': true, \'functionValue\': 0.0, \'functionEnabled\': false, \'function\': \'\', \'minimumVolumeChange\': 1.0E-8, \'minimumCellVolume\': 0.0, \'minimumCellQualityEnabled\': true, \'minimumCellQuality\': 1.0E-5, \'minimumDiscontiguousCells\': 1, \'minimumDiscontiguousCellsEnabled\': true, \'minimumFaceValidity\': 0.9}"));

   
//Run - 0 deg yaw
    Solution solution_0 = 
      simulation_0.getSolution();

    solution_0.clearSolution(Solution.Clear.History, Solution.Clear.Fields, Solution.Clear.LagrangianDem);

    simulation_0.getSimulationIterator().run();

 //Print all the final values of reports 
    
    FrontalAreaReport frontalAreaReport_0 = 
      ((FrontalAreaReport) simulation_0.getReportManager().getReport("Frontal Area_bottles"));

    frontalAreaReport_0.getParts().setQuery(null);

    
    Boundary boundary_50 = 
      region_1.getBoundaryManager().getBoundary("bar");

    Boundary boundary_51 = 
      region_1.getBoundaryManager().getBoundary("fork");

    Boundary boundary_52 = 
      region_1.getBoundaryManager().getBoundary("frame.frame");

    Boundary boundary_53 = 
      region_1.getBoundaryManager().getBoundary("frame.seatpost");

    Boundary boundary_54 = 
      region_1.getBoundaryManager().getBoundary("shock");

    Boundary boundary_55 = 
      region_1.getBoundaryManager().getBoundary("stem_ht_spacer");

    Boundary boundary_56 = 
      region_1.getBoundaryManager().getBoundary("waterbottle.DT");

    Boundary boundary_57 = 
      region_1.getBoundaryManager().getBoundary("wheel.front");

    Boundary boundary_58 = 
      region_1.getBoundaryManager().getBoundary("wheel.rear");

    Boundary boundary_59 = 
      region_1.getBoundaryManager().getBoundary("XC.number_plate");
      
    Boundary boundary_60 = 
      region_1.getBoundaryManager().getBoundary("mannequin");
      
    Boundary boundary_61 = 
      region_1.getBoundaryManager().getBoundary("waterbottle.ST");



    frontalAreaReport_0.getParts().setObjects(boundary_50, boundary_51, boundary_52, boundary_53, boundary_54, boundary_55, boundary_56, boundary_57, boundary_58, boundary_59, boundary_60);

    frontalAreaReport_0.printReport();

    ForceReport forceReport_70 = 
      ((ForceReport) simulation_0.getReportManager().getReport("Force_frame_by_parts"));

    forceReport_70.printReport();

    ForceCoefficientReport forceCoefficientReport_70 = 
      ((ForceCoefficientReport) simulation_0.getReportManager().getReport("Force_Coefficient"));

    forceCoefficientReport_70.printReport();

    forceCoefficientReport_70.getParts().setQuery(null);

    
    forceCoefficientReport_70.getParts().setObjects(boundary_50, boundary_51, boundary_52, boundary_53, boundary_54, boundary_55, boundary_56, boundary_61, boundary_57, boundary_58, boundary_59, boundary_60);

    forceCoefficientReport_70.printReport();

      
      
      // Extract drag by parts and save
  
    XyzInternalTable xyzInternalTable_0 = 
      ((XyzInternalTable) simulation_0.getTableManager().getTable("force_measurements"));

    xyzInternalTable_0.extract();  
    xyzInternalTable_0.export(dir + sep + filename+ "report _0deg.csv", ",");          

  // Extract accumulated drag x-y points
         
    AccumulatedForceTable accumulatedForceTable_1 = 
      ((AccumulatedForceTable) simulation_0.getTableManager().getTable("local_force")); // fork + frame + seatpost only

   FvRepresentation fvRepresentation_0 = 
      ((FvRepresentation) simulation_0.getRepresentationManager().getObject("Volume Mesh"));

    accumulatedForceTable_1.setRepresentation(fvRepresentation_0);
    
    accumulatedForceTable_1.extract();
    accumulatedForceTable_1.export(dir + sep + filename+ "local_force_0deg.csv", ",");

    AccumulatedForceTable accumulatedForceTable_0 = 
      ((AccumulatedForceTable) simulation_0.getTableManager().getTable("Accumulated Force Table")); // all parts in the bike
    
    accumulatedForceTable_0.setRepresentation(fvRepresentation_0);
    accumulatedForceTable_0.extract();
    accumulatedForceTable_0.export(dir + sep +  filename+ "acc_force_0deg.csv", ",");
       
    Scene scene_0 = 
      simulation_0.getSceneManager().getScene("Scalar Scene 1");
    
     scene_0.printAndWait(resolvePath(dir + sep + filename+ "image_0deg.png"), 1, 1267, 607, true, false);
     
    Scene scene_1 = 
      simulation_0.getSceneManager().getScene("Vector Scene 1");

    scene_1.export3DSceneFileAndWait(resolvePath(dir + sep + filename+ "xc_vector_scene.sce"), "Vector Scene 1", "", false, false);
    
   
 // } for debug
//}   for debug

   simulation_0.saveState(dir + sep + filename + "_0.sim"); 
 
// --------------------------------------------------------------------------------------------------------------------
      
//Rotate the outer region for 10 deg yaw
      
    

    LabCoordinateSystem labCoordinateSystem_0 = 
      simulation_0.getCoordinateSystemManager().getLabCoordinateSystem();

    Units units_5 = 
      simulation_0.getUnitsManager().getPreferredUnits(new IntVector(new int[] {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));

    simulation_0.getRepresentationManager().rotateMesh(new NeoObjectVector(new Object[] {region_0}), new DoubleVector(new double[] {0.0, 1.0, 0.0}), new NeoObjectVector(new Object[] {units_5, units_5, units_5}), -0.17453292519943298, labCoordinateSystem_0);

  // Turn on the LowRe Damping
    PhysicsContinuum physicsContinuum_0 = 
      ((PhysicsContinuum) simulation_0.getContinuumManager().getContinuum("Physics 1"));

    SstKwTurbModel sstKwTurbModel_0 = 
      physicsContinuum_0.getModelManager().getModel(SstKwTurbModel.class);

    sstKwTurbModel_0.setLowReDamping(true);
    
    
    //Adjust stopping criteria
   
    stepStoppingCriterion_0.setMaximumNumberSteps(itr_num+300);
    
  //Run - 10 deg yaw
    
   simulation_0.getSimulationIterator().run();

 //Print all the final values of reports 
      // Extract and save
      
    xyzInternalTable_0.extract(); 
    xyzInternalTable_0.export(dir + sep + filename+ "report _10deg.csv", ",");  
    
    accumulatedForceTable_1.setRepresentation(fvRepresentation_0);
    accumulatedForceTable_1.extract();
    accumulatedForceTable_1.export(dir + sep + filename+ "local_force_10deg.csv", ",");
    
    accumulatedForceTable_0.setRepresentation(fvRepresentation_0);
    accumulatedForceTable_0.extract();
    accumulatedForceTable_0.export(dir + sep + filename+ "acc_force_10deg.csv", ",");
   
    scene_0.printAndWait(resolvePath(dir + sep + filename + "image_10deg.png"), 1, 1267, 607, true, false);
    
    forceReport_70.printReport();
    forceCoefficientReport_70.printReport();
                        
  simulation_0.saveState(dir + sep + filename + "_10.sim"); 
//end macro 
    
  }
}
