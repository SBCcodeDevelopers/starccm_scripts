/* STAR-CCM+ macro: SL8_macro.java
 Written by STAR-CCM+ 15.02.007

 ******************************
*        Mio Suzuki 
*        November 7, 2020, v3 for ID model
*        
*        Procedure:
*        Geometry/Mesh operation
*                   * Replace operations on
*                                    * frame, fork, spacer, and seatpost
*                   * frame surface wrapper, cockpit_wrap
*                   * subtract in region 1 (inner region)
*                   * mesh region 1 (surface + volume)
*
*        Run (0 deg)
*        Reporting
*                   * consolidate all values in a single table. extract values. 
*                   * export this table to report.csv
*                   * extract and export local and acc force table to .csv
*                   * print the streamline + vorticity image 
*
*        Rotate the outer mesh by 10 deg + LowRe formulation
*                  * change the iteration stopping criteria
*                  * then execute the same run + reporting 
*                  * print the streamline + vorticity image 
*
* ******************************/

package macro;

import java.util.*;

import java.io.*;
import star.common.*;
import star.base.neo.*;
import star.surfacewrapper.*;
import star.meshing.*;
import star.flow.*;
import star.vis.*;
import star.kwturb.*;

import javax.swing.text.Document;
import org.netbeans.core.output2.ui.AbstractOutputPane;
import org.netbeans.core.output2.ui.AbstractOutputTab;
import org.openide.windows.TopComponent;
import org.netbeans.core.io.ui.IOWindow;

public class SL8_macro3 extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    String dir = simulation_0.getSessionDir(); 

    String sep = System.getProperty("file.separator");
    
    String filename = 
      simulation_0.getPresentationName(); 
    
    StepStoppingCriterion stepStoppingCriterion_0 = 
       ((StepStoppingCriterion) simulation_0.getSolverStoppingCriterionManager().getSolverStoppingCriterion("Maximum Steps"));
     
    int itr_num = 400; // max iteration number . For now, 250 for 0 and then 300 for 10
    
    stepStoppingCriterion_0.setMaximumNumberSteps(itr_num);
    
    
//* --------------------------------------------------------------------------------------------------------------------
//0 deg:
     
//Replace the CAD
    
// -------------------------------- new replace (7/7/2020) -----------------    
    
    /*
     CompositePart compositePart_0 = 
      ((CompositePart) simulation_0.get(SimulationPartManager.class).getPart("ASM0003_ASM"));

    CadPart cadPart_0 = 
      ((CadPart) compositePart_0.getChildParts().getPart("ID2_frame_fork"));

    cadPart_0.reimportPart(resolvePath("0000150723_v1.stp"), 0.0, false); */
     
     
     
     /*CompositePart compositePart_1 = 
      ((CompositePart) simulation_0.get(SimulationPartManager.class).getPart("SL8"));

    CompositePart compositePart_0 = 
      ((CompositePart) compositePart_1.getChildParts().getPart("0000150557_barstem"));

    CadPart cadPart_0 = 
      ((CadPart) compositePart_0.getChildParts().getPart("original_spacer"));

    //cadPart_0.reimportPart(resolvePath("HS-COVER.stp"), 0.0, false);

    CadPart cadPart_1 = 
      ((CadPart) compositePart_1.getChildParts().getPart("fork"));

    //cadPart_1.reimportPart(resolvePath("FORK.stp"), 0.0, false);
    
    
    CadPart cadPart_2 = 
      ((CadPart) compositePart_1.getChildParts().getPart("frame"));

    //cadPart_2.reimportPart(resolvePath("0000152631_DTTOP_182.stp"), 0.0, false); 
    
    CadPart cadPart_3 = 
      ((CadPart) compositePart_1.getChildParts().getPart("seat_post"));

    //cadPart_3.reimportPart(resolvePath("0000152631_DTTOP_182_SP.stp"), 0.0, false);  */
    
        
 
//Mesh operation    
      
    //Surface wrap: 1. frame wrap 2. bar_stem wrap
    //SurfaceWrapperAutoMeshOperation surfaceWrapperAutoMeshOperation_0 = 
     // ((SurfaceWrapperAutoMeshOperation) simulation_0.get(MeshOperationManager.class).getObject("frame_wrap"));

    //surfaceWrapperAutoMeshOperation_0.execute();


    //SurfaceWrapperAutoMeshOperation surfaceWrapperAutoMeshOperation_1 = 
     // ((SurfaceWrapperAutoMeshOperation) simulation_0.get(MeshOperationManager.class).getObject("bar_stem_wrap"));

    //surfaceWrapperAutoMeshOperation_1.getInputGeometryObjects().setQuery(null);

    //CompositePart compositePart_4 = 
     // ((CompositePart) simulation_0.get(SimulationPartManager.class).getPart("0000156973_ASM"));

    //MeshPart meshPart_19 = 
     // ((MeshPart) compositePart_4.getChildParts().getPart("bar_ASM1"));

   // CadPart cadPart_16 = 
   //  ((CadPart) compositePart_4.getChildParts().getPart("Garmin_simple"));

   // CadPart cadPart_19 = 
    //  ((CadPart) compositePart_4.getChildParts().getPart("mount_26"));

    //CadPart cadPart_14 = 
      //((CadPart) compositePart_4.getChildParts().getPart("spacers"));

    //surfaceWrapperAutoMeshOperation_1.getInputGeometryObjects().setObjects(meshPart_19,  cadPart_14);

    //surfaceWrapperAutoMeshOperation_1.execute();

    //Assign subtracted faces to designated boundaries
    //SubtractPartsOperation subtractPartsOperation_0 = 
      //((SubtractPartsOperation) simulation_0.get(MeshOperationManager.class).getObject("Subtract_region1"));

    //subtractPartsOperation_0.execute();
    
     //MeshOperationPart meshOperationPart_1 = 
      //((MeshOperationPart) simulation_0.get(SimulationPartManager.class).getPart("Subtract"));

    //PartSurface partSurface_29 = 
    //  ((PartSurface) meshOperationPart_1.getPartSurfaceManager().getPartSurface("bar_stem_wrap.0000156973_ASM.bar_ASM1.Faces"));

    //Region region_5 = 
     // simulation_0.getRegionManager().getRegion("Region_1_inner");

    //Boundary boundary_1 = 
     // region_5.getBoundaryManager().getBoundary("bar");  //Assign all stem + bar parts to boundary bar

    //artSurface_29.setBoundary(boundary_1);

    //PartSurface partSurface_30 = 
    //  ((PartSurface) meshOperationPart_1.getPartSurfaceManager().getPartSurface("bar_stem_wrap.0000156973_318.Garmin_simple.Faces"));

   // Boundary boundary_0 = 
    //  region_5.getBoundaryManager().getBoundary("garmin"); //Assign the mount + Garmin to boundary garmin

  //  partSurface_30.setBoundary(boundary_0);

 //   PartSurface partSurface_31 = 
  //    ((PartSurface) meshOperationPart_1.getPartSurfaceManager().getPartSurface("bar_stem_wrap.0000156973_318.mount_26.Faces"));

  //  partSurface_31.setBoundary(boundary_0);

    //PartSurface partSurface_32 = 
      //((PartSurface) meshOperationPart_1.getPartSurfaceManager().getPartSurface("bar_stem_wrap.0000156973_ASM.spacers.Faces"));

    //partSurface_32.setBoundary(boundary_1);
    

    //Mesh
    AutoMeshOperation autoMeshOperation_0 = 
      ((AutoMeshOperation) simulation_0.get(MeshOperationManager.class).getObject("mesh_region1"));

   autoMeshOperation_0.execute();

   
//Run - 0 deg yaw
    Solution solution_0 = 
      simulation_0.getSolution();

    solution_0.clearSolution(Solution.Clear.History, Solution.Clear.Fields, Solution.Clear.LagrangianDem);

    simulation_0.getSimulationIterator().run();

 //Print all the final values of reports 
    // Extract drag by parts and save
  
    XyzInternalTable xyzInternalTable_0 = 
      ((XyzInternalTable) simulation_0.getTableManager().getTable("force_measurements"));

    xyzInternalTable_0.extract();  
    xyzInternalTable_0.export(dir + sep + filename+ "report _0deg.csv", ",");          

  // Extract accumulated drag x-y points
         
    AccumulatedForceTable accumulatedForceTable_1 = 
      ((AccumulatedForceTable) simulation_0.getTableManager().getTable("local_force")); // fork + frame + seatpost only

   FvRepresentation fvRepresentation_0 = 
      ((FvRepresentation) simulation_0.getRepresentationManager().getObject("Volume Mesh"));

    accumulatedForceTable_1.setRepresentation(fvRepresentation_0);
    
    accumulatedForceTable_1.extract();
    accumulatedForceTable_1.export(dir + sep + filename+ "local_force_0deg.csv", ",");

    AccumulatedForceTable accumulatedForceTable_0 = 
      ((AccumulatedForceTable) simulation_0.getTableManager().getTable("Accumulated Force Table")); // all parts in the bike
    
    accumulatedForceTable_0.setRepresentation(fvRepresentation_0);
    accumulatedForceTable_0.extract();
    accumulatedForceTable_0.export(dir + sep +  filename+ "acc_force_0deg.csv", ",");
       
    Scene scene_0 = 
      simulation_0.getSceneManager().getScene("Scalar Scene 1");
    
     scene_0.printAndWait(resolvePath(dir + sep + filename+ "image_0deg.png"), 1, 1267, 607, true, false);
        
   
 // } for debug
//}   for debug

   simulation_0.saveState(dir + sep + filename + "_0.sim"); 
 
// --------------------------------------------------------------------------------------------------------------------
      
//Rotate the outer region for 10 deg yaw
      
    Region region_0 = 
      simulation_0.getRegionManager().getRegion("Region_2_outer");

    LabCoordinateSystem labCoordinateSystem_0 = 
      simulation_0.getCoordinateSystemManager().getLabCoordinateSystem();

    Units units_5 = 
      simulation_0.getUnitsManager().getPreferredUnits(new IntVector(new int[] {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));

    simulation_0.getRepresentationManager().rotateMesh(new NeoObjectVector(new Object[] {region_0}), new DoubleVector(new double[] {0.0, 1.0, 0.0}), new NeoObjectVector(new Object[] {units_5, units_5, units_5}), -0.17453292519943298, labCoordinateSystem_0);

  // Turn on the LowRe Damping
    PhysicsContinuum physicsContinuum_0 = 
      ((PhysicsContinuum) simulation_0.getContinuumManager().getContinuum("Physics 1"));

    SstKwTurbModel sstKwTurbModel_0 = 
      physicsContinuum_0.getModelManager().getModel(SstKwTurbModel.class);

    sstKwTurbModel_0.setLowReDamping(true);
    
    
    //Adjust stopping criteria
   
    stepStoppingCriterion_0.setMaximumNumberSteps(itr_num+300);
    
  //Run - 10 deg yaw
    
   simulation_0.getSimulationIterator().run();

 //Print all the final values of reports 
      // Extract and save
      
    xyzInternalTable_0.extract(); 
    xyzInternalTable_0.export(dir + sep + filename+ "report _10deg.csv", ",");  
    
    accumulatedForceTable_1.setRepresentation(fvRepresentation_0);
    accumulatedForceTable_1.extract();
    accumulatedForceTable_1.export(dir + sep + filename+ "local_force_10deg.csv", ",");
    
    accumulatedForceTable_0.setRepresentation(fvRepresentation_0);
    accumulatedForceTable_0.extract();
    accumulatedForceTable_0.export(dir + sep + filename+ "acc_force_10deg.csv", ",");
   
    scene_0.printAndWait(resolvePath(dir + sep + filename + "image_10deg.png"), 1, 1267, 607, true, false);
                        
  simulation_0.saveState(dir + sep + filename + "_10.sim"); 
//end macro 
    
  }
}
