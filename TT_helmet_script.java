/* STAR-CCM+ macro: helmet_script.java
 STAR-CCM+ 15.02.007

 ******************************
*        Mio Suzuki 
*        March 15, 2021
*        
*        Procedure:
*        Geometry/Mesh operation
*                   * Replace operations on helmet
*                   * frame surface wrapper, cockpit_wrap
*                   * subtract in region 1 (inner region)
*                   * mesh region 1 (surface + volume)
*
*        Run (0 deg)
*        Reporting: Rolling average of drag and temperature 
*
***********************************/

package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.vis.*;
import star.meshing.*;
import star.surfacewrapper.*;
import star.base.report.*;
import star.flow.*;
import java.io.*;

import javax.swing.text.Document;
import org.netbeans.core.output2.ui.AbstractOutputPane;
import org.netbeans.core.output2.ui.AbstractOutputTab;
import org.openide.windows.TopComponent;
import org.netbeans.core.io.ui.IOWindow;


public class TT_helmet_script extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();
    
    String dir = simulation_0.getSessionDir(); 

    String sep = System.getProperty("file.separator");
    
    String filename = 
      simulation_0.getPresentationName(); 
    
    
     /* ---------------------------------------------------------------------
        * Geometry Operation: 
                      Replace the helmet with the new CAD:  */
    
    CompositePart compositePart_0 = 
      ((CompositePart) simulation_0.get(SimulationPartManager.class).getPart("Helmet"));

    CadPart cadPart_0 = 
      ((CadPart) compositePart_0.getChildParts().getPart("helmet"));

    cadPart_0.reimportPart(resolvePath("tt4-2021-cfd.stp"), 0.0, false);
    
    /*
    //Rotate the helmet by 25.4 deg
    
    Units units_0 = 
      simulation_0.getUnitsManager().getPreferredUnits(new IntVector(new int[] {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));

    Units units_1 = 
      simulation_0.getUnitsManager().getPreferredUnits(new IntVector(new int[] {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));

    LabCoordinateSystem labCoordinateSystem_0 = 
      simulation_0.getCoordinateSystemManager().getLabCoordinateSystem();

    CartesianCoordinateSystem cartesianCoordinateSystem_0 = 
      ((CartesianCoordinateSystem) labCoordinateSystem_0.getLocalCoordinateSystemManager().getObject("manneqiun_road"));

    compositePart_0.getChildParts().rotateParts(new NeoObjectVector(new Object[] {cadPart_0}), new DoubleVector(new double[] {1.0, 0.0, 0.0}), new NeoObjectVector(new Object[] {units_1, units_1, units_1}), 0.44331363000655977, cartesianCoordinateSystem_0);
    */
    
   /* ---------------------------------------------------------------------
        * Mesh Operation: 
                      * Surface wrap
                      * Cut
                      * Automatic mesh
    -------------------------------------------------------------------------- */
    
    SurfaceWrapperAutoMeshOperation surfaceWrapperAutoMeshOperation_0 = 
      ((SurfaceWrapperAutoMeshOperation) simulation_0.get(MeshOperationManager.class).getObject("Surface Wrapper"));

     surfaceWrapperAutoMeshOperation_0.execute();

    SubtractPartsOperation subtractPartsOperation_0 = 
      ((SubtractPartsOperation) simulation_0.get(MeshOperationManager.class).getObject("Road_Subtract"));

    subtractPartsOperation_0.execute();

    AutoMeshOperation autoMeshOperation_0 = 
      ((AutoMeshOperation) simulation_0.get(MeshOperationManager.class).getObject("Automated Mesh"));

    autoMeshOperation_0.execute();

   //Clear Solution

   Solution solution_0 = 
      simulation_0.getSolution();

    solution_0.clearSolution(Solution.Clear.History, Solution.Clear.Fields, Solution.Clear.LagrangianDem);

    //Stopping Criteria --> 500
    StepStoppingCriterion stepStoppingCriterion_0 = 
      ((StepStoppingCriterion) simulation_0.getSolverStoppingCriterionManager().getSolverStoppingCriterion("Maximum Steps"));

    stepStoppingCriterion_0.setMaximumNumberSteps(700);

    //Run
    simulation_0.getSimulationIterator().run();

    /* ---------------------------------------------------------------------
        * Save the accumulated plots */
    
    //Print all the final values of reports 
    // Extract drag by parts and save
  
    XyzInternalTable xyzInternalTable_0 = 
      ((XyzInternalTable) simulation_0.getTableManager().getTable("force_measurements"));

    xyzInternalTable_0.extract();  
    xyzInternalTable_0.export(dir + sep + filename+ "report _0deg.csv", ",");          

  // Extract accumulated drag x-y points
         
    
   FvRepresentation fvRepresentation_0 = 
      ((FvRepresentation) simulation_0.getRepresentationManager().getObject("Volume Mesh"));

    AccumulatedForceTable accumulatedForceTable_0 = 
      ((AccumulatedForceTable) simulation_0.getTableManager().getTable("Overall_acc")); 
    
    accumulatedForceTable_0.setRepresentation(fvRepresentation_0);
    accumulatedForceTable_0.extract();
    accumulatedForceTable_0.export(dir + sep +  filename+ "acc_force_0deg.csv", ",");
       
    Scene scene_0 = 
      simulation_0.getSceneManager().getScene("Helmet_drag");
    
     scene_0.printAndWait(resolvePath(dir + sep + filename+ "image_0deg.png"), 1, 1350, 570, true, false);
 // end  macro
  
  } //execute0
  
  
}