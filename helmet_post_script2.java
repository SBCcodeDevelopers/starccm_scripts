// Simcenter STAR-CCM+ macro: helmet_post_script2.java
// Written by Simcenter STAR-CCM+ 15.06.008
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.vis.*;
import star.meshing.*;

public class helmet_post_script2 extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = 
      getActiveSimulation();

    Scene scene_0 = 
      simulation_0.getSceneManager().getScene("Helmet_drag");

    ScalarDisplayer scalarDisplayer_0 = 
      ((ScalarDisplayer) scene_0.getDisplayerManager().getDisplayer("helmet_pressure"));

    scalarDisplayer_0.setVisibilityOverrideMode(DisplayerVisibilityOverride.USE_PART_PROPERTY);

    FvRepresentation fvRepresentation_0 = 
      ((FvRepresentation) simulation_0.getRepresentationManager().getObject("Volume Mesh"));

    scalarDisplayer_0.setRepresentation(fvRepresentation_0);

    StreamDisplayer streamDisplayer_0 = 
      ((StreamDisplayer) scene_0.getDisplayerManager().getDisplayer("helmet_Streamlines"));

    streamDisplayer_0.setVisibilityOverrideMode(DisplayerVisibilityOverride.USE_PART_PROPERTY);

    streamDisplayer_0.setRepresentation(fvRepresentation_0);

    PartDisplayer partDisplayer_0 = 
      ((PartDisplayer) scene_0.getDisplayerManager().getDisplayer("mannequin_body"));

    Region region_0 = 
      simulation_0.getRegionManager().getRegion("Region");

    Boundary boundary_0 = 
      region_0.getBoundaryManager().getBoundary("Helmet");

    Boundary boundary_1 = 
      region_0.getBoundaryManager().getBoundary("Subtract.Manny.head_top");

    Boundary boundary_2 = 
      region_0.getBoundaryManager().getBoundary("Subtract.Manny.Manny");

    partDisplayer_0.getInputParts().eraseObjects(boundary_0, boundary_1, boundary_2);

    CompositePart compositePart_0 = 
      ((CompositePart) simulation_0.get(SimulationPartManager.class).getPart("Road"));

    CadPart cadPart_0 = 
      ((CadPart) compositePart_0.getChildParts().getPart("mannequin_road_upper"));

    PartSurface partSurface_0 = 
      ((PartSurface) cadPart_0.getPartSurfaceManager().getPartSurface("arm.left"));

    PartSurface partSurface_1 = 
      ((PartSurface) cadPart_0.getPartSurfaceManager().getPartSurface("arm.right"));

    PartSurface partSurface_2 = 
      ((PartSurface) cadPart_0.getPartSurfaceManager().getPartSurface("head_top"));

    PartSurface partSurface_3 = 
      ((PartSurface) cadPart_0.getPartSurfaceManager().getPartSurface("face"));

    PartSurface partSurface_4 = 
      ((PartSurface) cadPart_0.getPartSurfaceManager().getPartSurface("torso"));

    partDisplayer_0.getVisibleParts().addParts(partSurface_0, partSurface_1, partSurface_2, partSurface_3, partSurface_4);

    partDisplayer_0.getHiddenParts().addParts();

    PartRepresentation partRepresentation_0 = 
      ((PartRepresentation) simulation_0.getRepresentationManager().getObject("Geometry"));

    partDisplayer_0.setRepresentation(partRepresentation_0);

    StreamDisplayer streamDisplayer_1 = 
      ((StreamDisplayer) scene_0.getDisplayerManager().getDisplayer("mannequin_Surface_stream"));

    streamDisplayer_1.setRepresentation(fvRepresentation_0);

    StreamDisplayer streamDisplayer_2 = 
      ((StreamDisplayer) scene_0.getDisplayerManager().getDisplayer("Streamlines 1"));

    StreamPart streamPart_0 = 
      ((StreamPart) simulation_0.getPartManager().getObject("sideR_Streamline"));

    StreamPart streamPart_1 = 
      ((StreamPart) simulation_0.getPartManager().getObject("Streamline"));

    StreamPart streamPart_2 = 
      ((StreamPart) simulation_0.getPartManager().getObject("sideL_Streamline"));

    streamDisplayer_2.getVisibleParts().addParts(streamPart_0, streamPart_1, streamPart_2);

    streamDisplayer_2.getHiddenParts().addParts();

    streamDisplayer_2.setRepresentation(fvRepresentation_0);

    streamDisplayer_2.setOpacity(0.1);

    streamDisplayer_2.setColorMode(StreamColorMode.CONSTANT);

    streamDisplayer_2.setWidth(0.001);

    
    CurrentView currentView_0 = 
      scene_0.getCurrentView();

    VisView visView_0 = 
      ((VisView) simulation_0.getViewManager().getObject("View 6"));

    currentView_0.setView(visView_0);

    Legend legend_0 = 
      scalarDisplayer_0.getLegend();

    legend_0.updateLayout(new DoubleVector(new double[] {0.31864373954634906, 0.828498146274227}), 0.5999999999999982, 0.04399999999999904, 0);

    legend_0.updateLayout(new DoubleVector(new double[] {0.31864373954634906, 0.828498146274227}), 0.2452830188679228, 0.04399999999999904, 0);

    legend_0.updateLayout(new DoubleVector(new double[] {0.6001531735086136, 0.855363817916018}), 0.24528301886792314, 0.04399999999999904, 0);

    ScalarDisplayer scalarDisplayer_1 = 
      ((ScalarDisplayer) scene_0.getDisplayerManager().getDisplayer("data_focus_thermal"));

    Legend legend_1 = 
      scalarDisplayer_1.getLegend();

    legend_1.updateLayout(new DoubleVector(new double[] {0.8945310014965225, 0.17676622412797885}), 0.04400000000000015, 0.5999999999999993, 1);

    legend_1.updateLayout(new DoubleVector(new double[] {0.8945310014965225, 0.17676622412797885}), 0.04400000000000015, 0.24029850746268552, 1);

    legend_1.updateLayout(new DoubleVector(new double[] {0.909625341119164, 0.7125871196503669}), 0.04399999999999993, 0.24029850746268555, 1);

    scene_0.printAndWait(resolvePath("C:\\_workspace\\Helemt\\EV3_project\\side_profile.png"), 1, 1326, 671, true, false);

    VisView visView_1 = 
      ((VisView) simulation_0.getViewManager().getObject("View 5"));

    currentView_0.setView(visView_1);

    streamDisplayer_2.setVisibilityOverrideMode(DisplayerVisibilityOverride.HIDE_ALL_PARTS);

    scalarDisplayer_1.setVisibilityOverrideMode(DisplayerVisibilityOverride.HIDE_ALL_PARTS);

    scene_0.printAndWait(resolvePath("C:\\_workspace\\Helemt\\EV3_project\\front.png"), 1, 1326, 671, true, false);

    VisView visView_2 = 
      ((VisView) simulation_0.getViewManager().getObject("View 3"));

    currentView_0.setView(visView_2);

    scene_0.printAndWait(resolvePath("C:\\_workspace\\Helemt\\EV3_project\\top.png"), 1, 1326, 671, true, false);
  }
}
